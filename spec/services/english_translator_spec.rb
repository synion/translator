require 'spec_helper'


RSpec.describe EnglishTranslator do
  let (:english) { EnglishTranslator.new}
  let (:word) { 'kot' }
  let (:translator) { english.call(word) }
  it "Polish word give english word" do
    expect(translator) .eql?("cat")
  end
  it "EnglishTrasnlator be array" do
    expect(translator).to be_kind_of(String)
  end
end


