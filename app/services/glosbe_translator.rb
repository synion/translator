require 'net/http'
require 'json'

class GlosbeTranslator

    def initialize(word,from,to)
      @word = word
      @from = from
      @to   = to
    end

    def to_s
      JSON.parse(response)
    end

    def word
      word = URI.escape(@word)
    end

    def response
      url = "https://glosbe.com/gapi/translate?from=#{@from}&dest=#{@to}&format=json&phrase=#{word}&pretty=true"
      uri = URI(url)
      response = Net::HTTP.get(uri)
    end
end
