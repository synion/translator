$( document ).ready(function(){
	  $('.button-collapse').sideNav({
	    menuWidth: 300,
	    edge: 'left',
	    closeOnClick: true,
	    draggable: true
	  }
	);

      $('.parallax').parallax();
      $('.slider').slider({
        indicators:false,
        interval: 3000
      });
     $('.modal-trigger').leanModal();
     $('select').material_select();
     $(".dropdown-button").dropdown();
     $('#about_map').dialog({
      close:function(){
      alert('Take you drive')
      }
    });
});
