class EnglishTranslatorController <  ActionController::Base

  def new
    english_translator = EnglishTranslator.new
    @result = english_translator.call(params[:word])
    redirect_to controller: :home, action: :index, result: @result
  end

  private

  def english_params
    params.require(:english).permit(:word)
  end

end
