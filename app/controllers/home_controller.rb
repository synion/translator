
class HomeController < ApplicationController
	def index
    @result = (params[:result])
	end

  private
  def home_params
    params.require(:home).permit(:result)
  end
end
